<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep=new Animal('shaun');
echo "Nama Domba : $sheep->name <br>";
echo "Jumlah kaki: $sheep->leg <br>";
echo "Berdarah dingin: $sheep->get_cold_blooded <br><br>";

$sungokong=new Ape('kera sakti');
echo "Nama Kera : $sungokong->name <br>";
echo "Jumlah kaki: $sungokong->leg <br>";
$sungokong->yell();

$kodok=new Frog('buduk');
echo "Nama Kodok : $kodok->name <br>";
echo "Jumlah kaki: $kodok->leg <br>";
$kodok->jump();

?>